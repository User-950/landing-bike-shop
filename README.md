# Landing-bike-shop

The main purpose of the project is to practice make site for selling an electro bike. The project itself is a landing page website about electro bike. The site used interactive elements from different libraries.

What is used:

- Programming languages: HTML, CSS, JavaScript;
- Libraries: jQuery, Slick;

To clone repository

```shell
git clone https://gitlab.com/User-950/landing-bike-shop
```

To install packages, use the command

```shell
npm install
```